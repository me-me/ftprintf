/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/13 14:30:49 by abnaceur          #+#    #+#             */
/*   Updated: 2016/12/13 15:27:48 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *s1, const char *s2)
{
	const char *ts1;
	const char *ts2;

	if (*s2 == '\0')
		return ((char*)s1);
	while (*s1 != '\0')
	{
		ts1 = s1;
		ts2 = s2;
		while (*ts2 != '\0' && *ts1 == *ts2)
		{
			ts1++;
			ts2++;
		}
		if (*ts2 == '\0')
			return ((char*)s1);
		s1++;
	}
	return (NULL);
}
