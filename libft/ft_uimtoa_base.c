/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_uimtoa_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/23 07:50:22 by abnaceur          #+#    #+#             */
/*   Updated: 2017/04/23 07:51:25 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_uimtoa_base(uintmax_t nb, int base)
{
	char		*res;
	int			size;
	const char	vals[] = "0123456789abcdef";

	size = ft_nblenbase(nb, base);
	if (!(res = ft_strnew(size)))
		return (NULL);
	while (size)
	{
		res[--size] = vals[nb % base];
		nb /= base;
	}
	return (res);
}
