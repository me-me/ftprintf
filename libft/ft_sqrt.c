/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sqrt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/23 08:35:18 by abnaceur          #+#    #+#             */
/*   Updated: 2017/04/23 08:35:33 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_sqrt(int nb)
{
	int sol;

	if (nb <= 0)
		return (0);
	sol = 1;
	while (sol * sol < nb)
		sol++;
	if (sol * sol == nb)
		return (sol);
	else
		return (0);
}
