# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/04/23 08:31:16 by abnaceur          #+#    #+#              #
#    Updated: 2017/04/26 07:57:51 by abnaceur         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a

CFLAGS = -Wall -Wextra -Werror

SRC  = libft/ft_memset.c \
	   libft/ft_bzero.c \
	   libft/ft_memcpy.c \
	   libft/ft_memccpy.c \
	   libft/ft_memmove.c \
	   libft/ft_memchr.c \
	   libft/ft_memcmp.c \
	   libft/ft_strlen.c \
	   libft/ft_strdup.c \
	   libft/ft_strcpy.c \
	   libft/ft_strncpy.c \
	   libft/ft_strcat.c \
	   libft/ft_strncat.c \
	   libft/ft_strlcat.c \
	   libft/ft_strchr.c \
	   libft/ft_strrchr.c \
	   libft/ft_strstr.c \
	   libft/ft_strnstr.c \
	   libft/ft_strcmp.c \
	   libft/ft_strncmp.c \
	   libft/ft_atoi.c \
	   libft/ft_isalpha.c \
	   libft/ft_isdigit.c \
	   libft/ft_isalnum.c \
	   libft/ft_isascii.c \
	   libft/ft_isprint.c \
	   libft/ft_toupper.c \
	   libft/ft_tolower.c \
	   libft/ft_memalloc.c \
	   libft/ft_memdel.c \
	   libft/ft_strnew.c \
	   libft/ft_strdel.c \
	   libft/ft_strclr.c \
	   libft/ft_striter.c \
	   libft/ft_striteri.c \
	   libft/ft_strmap.c \
	   libft/ft_strmapi.c \
	   libft/ft_strequ.c \
	   libft/ft_strnequ.c \
	   libft/ft_strsub.c \
	   libft/ft_strjoin.c \
	   libft/ft_strjoinfree.c \
	   libft/ft_strtrim.c \
	   libft/ft_strsplit.c \
	   libft/ft_pow.c \
	   libft/ft_itoa.c \
	   libft/ft_ftoa.c \
	   libft/ft_itoa_base.c \
	   libft/ft_imtoa_base.c \
	   libft/ft_uimtoa_base.c \
	   libft/ft_putchar.c \
	   libft/ft_putstr.c \
	   libft/ft_putendl.c \
	   libft/ft_putnbr.c \
	   libft/ft_putchar_fd.c \
	   libft/ft_putstr_fd.c \
	   libft/ft_putendl_fd.c \
	   libft/ft_putnbr_fd.c \
	   libft/ft_lstnew.c \
	   libft/ft_lstdelone.c \
	   libft/ft_lstdel.c \
	   libft/ft_lstadd.c \
	   libft/ft_lstiter.c \
	   libft/ft_lstmap.c \
	   libft/ft_lstpushback.c \
	   libft/ft_lstshift.c \
	   libft/ft_nblen.c \
	   libft/ft_nblenbase.c \
	   libft/ft_sqrt.c \
	   libft/ft_swapbits.c \
	   libft/ft_printbits.c \
	   libft/get_next_line.c \
	   src/ft_printf.c \
	   src/split_args.c \
	   src/init_pf.c \
	   src/arg_to_buffer.c \
	   src/check_arg.c \
	   src/pf_tools.c \
	   src/handlers/pf_handler_di.c \
	   src/handlers/pf_handler_uox.c \
	   src/handlers/pf_handler_s.c \
	   src/handlers/pf_handler_p.c \
	   src/handlers/pf_handler_c.c \
	   src/handlers/pf_handler_lc.c \
	   src/handlers/pf_handler_ls.c \
	   src/handlers/pf_handler_percent.c \
	   src/handlers/pf_handler_b.c \
	   src/handlers/pf_handler_f.c \
	   src/handlers/pf_handler_other.c



OBJ = $(SRC:.c=.o)

all: $(NAME)

%.o: %.c
	@gcc $(CFLAGS) -c $< -I includes -o $@

$(NAME): $(OBJ)
	@ar rc $(NAME) $(OBJ)
	@ranlib $(NAME)
	@printf '\033[32m----> Printf compiled \033[33m    [OK]\n'

clean:
	@/bin/rm -f $(OBJ)
	@printf '\033[32m----> Printf clean \033[33m       [OK]\n'

fclean: clean
	@/bin/rm -f $(NAME)
	@printf '\033[32m----> Printf full clean \033[33m  [OK]\n'

re: fclean all
