/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   split_args.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/23 09:03:10 by abnaceur          #+#    #+#             */
/*   Updated: 2017/04/26 08:15:39 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft/libft.h"

static int	find_arg(t_pf *pf, const char *format)
{
	int		len;
	t_arg	arg;

	if ((len = 1) && *format == '%')
	{
		while (format[len] && (ft_isdigit(format[len]) ||
			ft_strchr("*.", format[len]) || ft_strchr(LM, format[len])
					|| ft_strchr(FLAGS, format[len])))
			len++;
		len = format[len] ? len + 1 : len;
	}
	else
		while (format[len] && format[len] != '%')
			len++;
	ft_memset(&arg, 0, sizeof(t_arg));
	arg.text = ft_strsub(format, 0, len);
	if (*format == '%')
		arg.conversion = format[(len > 1 ? len - 1 : len)];
	else
		arg.val = format[len - 1];
	pf->arg = &arg;
	pf->ret = arg_to_buffer(pf);
	ft_strdel(&(arg.text));
	return (len);
}

void		split_args(t_pf *pf, const char *format)
{
	while (*format && pf->ret >= 0)
	{
		format += find_arg(pf, format);
	}
}
