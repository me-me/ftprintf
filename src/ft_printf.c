/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/23 09:02:12 by abnaceur          #+#    #+#             */
/*   Updated: 2017/04/23 10:37:36 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft/libft.h"

int			ft_printf(const char *format, ...)
{
	int			ret;
	t_pf		pf;
	va_list		ap;

	init_pf(&pf);
	pf.ap = &ap;
	va_start(*(pf.ap), format);
	split_args(&pf, format);
	va_end(*(pf.ap));
	if (pf.ret != -1)
		write(1, pf.buf, pf.ret);
	ret = pf.ret;
	ft_strdel(&(pf.buf));
	return (ret);
}
