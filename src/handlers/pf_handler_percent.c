/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pf_handler_percent.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/23 09:06:04 by abnaceur          #+#    #+#             */
/*   Updated: 2017/04/23 09:55:24 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../libft/libft.h"

static void		pf_helper2_percent(t_arg *arg, int dw)
{
	arg->buf = ft_strnew(arg->len);
	while (!arg->minus && dw--)
		ft_strcat(arg->buf, " ");
	ft_strcat(arg->buf, "%");
	while (arg->minus && dw--)
		ft_strcat(arg->buf, " ");
}

static void		pf_helper_percent(t_arg *arg)
{
	int		dwidth;

	dwidth = 0;
	arg->len = 1;
	if (arg->min_width > arg->len)
	{
		dwidth = arg->min_width - arg->len;
		arg->len = arg->min_width;
	}
	pf_helper2_percent(arg, dwidth);
}

char			*pf_handler_percent(t_arg *arg)
{
	arg->val = '%';
	if (!arg->conversion)
		pf_helper_percent(arg);
	return (pf_handler_c(arg));
}
