/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pf_tools.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/23 09:02:51 by abnaceur          #+#    #+#             */
/*   Updated: 2017/04/23 09:51:30 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft/libft.h"

char		*pf_join(t_pf *pf, t_arg *arg)
{
	char	*buf;

	if (!pf->buf || !arg->buf)
		return (NULL);
	if (!(buf = ft_strnew(pf->ret + arg->len)))
		return (NULL);
	ft_memcpy(buf, pf->buf, pf->ret);
	ft_memcpy(buf + pf->ret, arg->buf, arg->len);
	ft_strdel(&(arg->buf));
	ft_strdel(&(pf->buf));
	return (buf);
}
